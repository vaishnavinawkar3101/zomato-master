"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.OrderModel = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

// import mongoose from "mongoose";
// const OrderSchema = new mongoose.Schema({
//   user: {
//     type: mongoose.Types.ObjectId,
//     ref: "Users",
//   },
//   orderDetails: [
//     {
//       food: { type: mongoose.Types.ObjectId, ref: "Foods" },
//       quantity: { types: Number, required: true },
//       paymode: { type: String, required: true },
//       status: { type: String, default: "Placed" },
//       paymentDetails: {
//         itemTotal: { type: Number, required: true },
//         promo: { type: Number, reuired: true },
//         tax: { type: Number, reuired: true },
//       }
//     }]
// },
// {
//     timestamps: true
//     //storing the day and time when the order is post
// }
// );
// export const OrderModel = mongoose.model("Orders", OrderSchema);
var OrderSchema = new _mongoose["default"].Schema({
  user: {
    type: _mongoose["default"].Types.ObjectId,
    ref: "Users"
  },
  orderDetails: [{
    food: {
      type: _mongoose["default"].Types.ObjectId,
      ref: "Foods"
    },
    quantity: {
      type: Number,
      required: true
    },
    paymode: {
      type: String,
      required: true
    },
    status: {
      type: String,
      "default": "Placed"
    },
    paymentDetails: {
      itemTotal: {
        type: Number,
        required: true
      },
      promo: {
        type: Number,
        required: true
      },
      tax: {
        type: Number,
        required: true
      },
      razorpay_payment_id: {
        type: String,
        required: true
      }
    }
  }]
}, {
  timestamps: true
});

var OrderModel = _mongoose["default"].model("Orders", OrderSchema);

exports.OrderModel = OrderModel;