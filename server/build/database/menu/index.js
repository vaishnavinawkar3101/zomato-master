"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MenuModel = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

// import mongoose from "mongoose";
// const MenuSchema = new mongoose.Schema({
//   menus: [
//     {
//       name: { type: String, required: true },
//       items: [{ type: mongoose.Types.ObjectId, ref: "Foods" }],
//     },
//   ],
//   recommanded: [{ type: mongoose.Types.ObjectId, ref: "Foods", unique: true }],
// });
// export const MenuModel = mongoose.model("Menus", MenuSchema);
var MenuSchema = new _mongoose["default"].Schema({
  menus: [{
    name: {
      type: String,
      required: true
    },
    items: [{
      type: _mongoose["default"].Types.ObjectId,
      ref: "Foods"
    }]
  }],
  recommended: [{
    type: _mongoose["default"].Types.ObjectId,
    ref: "Foods",
    unique: true
  }]
}, {
  timestamps: true
});

var MenuModel = _mongoose["default"].model("Menus", MenuSchema);

exports.MenuModel = MenuModel;