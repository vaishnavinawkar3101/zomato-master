"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ImageModel = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

// import  mongoose  from "mongoose";
// const ImageSchema =new mongoose.Schema({
//   images: [{location : {type: String, required: true}}]
// });
// export const ImageModel = mongoose.model("Images", ImageSchema);
var ImageSchema = new _mongoose["default"].Schema({
  images: [{
    location: {
      type: String,
      required: true
    }
  }]
}, {
  timestamps: true
});

var ImageModel = _mongoose["default"].model("Image", ImageSchema);

exports.ImageModel = ImageModel;