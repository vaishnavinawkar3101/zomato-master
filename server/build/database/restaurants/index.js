"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.RestaurantModel = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

// import mongoose from "mongoose";
// const RestaurantSchema = new mongoose.Schema(
//     {
//         name: {type: String , required : true},
//         city: { type: String, reuired: true},
//         address: { type: String, reuired: true},
//         maploaction: { type: String, reuired: true},
//         cuisine: [String],
//         restaurantTimings: String,
//         contactNumber: Number,
//         website: String,
//         popularDishes: [String],
//         averageCost: Number,
//         amenities: [String],
//         menuImages: {
//         type: mongoose.Types.ObjectId, 
//         ref: "Images",
//         },
//         menu: {
//             type:  mongoose.Types.ObjectId,
//             ref: "Menus"
//         },
//         reviews:[{type: mongoose.Types.ObjectId,ref: "Reviews" }],
//         photos:{ type:  mongoose.Types.ObjectId, ref: "Images" },
//    },
//     {
//        timestamps: true,
//     }
// );
// export const RestaurantModel = mongoose.model("Restaurants", RestaurantSchema);
var RestaurantSchema = new _mongoose["default"].Schema({
  name: {
    type: String,
    required: true
  },
  city: {
    type: String,
    required: true
  },
  address: {
    type: String,
    required: true
  },
  mapLocation: {
    type: String,
    required: true
  },
  cuisine: [String],
  restaurantTimings: String,
  contactNumber: Number,
  website: String,
  popularDishes: [String],
  averageCost: Number,
  amenties: [String],
  menuImages: {
    type: _mongoose["default"].Types.ObjectId,
    ref: "Images"
  },
  menu: {
    type: _mongoose["default"].Types.ObjectId,
    ref: "Menus"
  },
  reviews: [{
    type: _mongoose["default"].Types.ObjectId,
    ref: "Reviews"
  }],
  photos: {
    type: _mongoose["default"].Types.ObjectId,
    ref: "Images"
  }
}, {
  timestamps: true
});

var RestaurantModel = _mongoose["default"].model("Restaurants", RestaurantSchema);

exports.RestaurantModel = RestaurantModel;