"use strict";

var _express = _interopRequireDefault(require("express"));

var _cors = _interopRequireDefault(require("cors"));

var _helmet = _interopRequireDefault(require("helmet"));

var _passport = _interopRequireDefault(require("passport"));

var _connection = _interopRequireDefault(require("./database/connection"));

var _google = _interopRequireDefault(require("./config/google.config"));

var _route = _interopRequireDefault(require("./config/route.config"));

var _Auth = _interopRequireDefault(require("./API/Auth"));

var _Restaurant = _interopRequireDefault(require("./API/Restaurant"));

var _Food = _interopRequireDefault(require("./API/Food"));

var _Menu = _interopRequireDefault(require("./API/Menu"));

var _Image = _interopRequireDefault(require("./API/Image"));

var _Orders = _interopRequireDefault(require("./API/Orders"));

var _Reviews = _interopRequireDefault(require("./API/Reviews"));

var _User = _interopRequireDefault(require("./API/User"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

//babel is the compiler of javascript it convert the es6 version of js to the es5
// import {UserModel}  from "./database/allModels";
//  const UserModel = require("./database/user");
// const mongoose =require('mongoose');
// require("@babel/core").transform("code",{
//     presets: ["@babel/preset-env"],
// });
require('dotenv').config(); //this all are the old version ES5
// const express = require("express");
// const cors = require('cors');
// const helmet = require("helmet");
//helmet is securing the express application by giviang the http headezomato
// const mongoose =require('mongoose');
//now we are writting the same with the ES6


//passport config
(0, _google["default"])(_passport["default"]);
(0, _route["default"])(_passport["default"]);
var zomato = (0, _express["default"])();
zomato.use((0, _cors["default"])());
zomato.use(_express["default"].json());
zomato.use((0, _helmet["default"])());
zomato.use(_passport["default"].initialize()); // zomato.use(passport.session());
//Application Routes
// it means that if you go to the
// localhost:4000/auth/singup it directly go to the auth API

zomato.use("/auth", _Auth["default"]);
zomato.use("/restaurant", _Restaurant["default"]);
zomato.use("/food", _Food["default"]);
zomato.use("/menu", _Menu["default"]);
zomato.use("/image", _Image["default"]);
zomato.use("/order", _Orders["default"]);
zomato.use("/review", _Reviews["default"]);
zomato.use("/user", _User["default"]);
zomato.listen(4000, function () {
  //this function is a promise function
  (0, _connection["default"])().then(function () {
    //.then is that happening at the success
    console.log("Server is running !!!");
  })["catch"](function (error) {
    //.catch for error
    console.log("Server is running , but database connect failed ....");
    console.log(error);
  });
});